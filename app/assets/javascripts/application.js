// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .


// --------------------------------------------- //
// --------   FONCTION FLASH MSG     ----------- //
// --------------------------------------------- //
var flashMessage = function(flash, message, statut){
    var notice = "<div class='notice notice-"+ statut +"' id='notice'>"+ message +"</div>";
    $('#main').prepend(notice);

    setInterval(function(){
        $('#notice').hide('slow');
    }, 3000);
};

// --------------------------------------------- //
// --------   FONCTION ALIMENTE TODO ----------- //
// --------------------------------------------- //
var appendToTodolist = function (todolist, data) {
    var obj = '';

    todolist.empty();

    if(data.length > 0) {
        for(var i = 0 ; i < data.length ; i++) {
            if(data[i].is_archived == false){
                obj += '<div class="action" id="todo-'+ data[i].id +'" data-key="'+ data[i].id +'" data-priority="'+ data[i].priority_id +'">';
                obj += '<input type="checkbox" name="checkbox" id="checkbox-'+ data[i].id +'">';
                obj += '<label for="checkbox-'+ data[i].id +'">'+ data[i].contenu +'</label>';
                obj += '</div>';
            }
        }
    }
    if(!obj.length > 0){
        obj += '<div class="action">Pas d\'action(s) en cours !</div>'
    }

    todolist.append(obj);
};

// --------------------------------------------- //
// -----  FONCTION ALIMENTE PROJET DETAILS ----- //
// --------------------------------------------- //
var appendToProjetDetail = function(aside, data, admin, userid) {
    var obj                 = '',
        objAdmin            = '',
        asideCollaborators  = aside.find('#collaborators');

    for(var i = 0; i < data.length ; i++){
        if(data[i].id == admin){
            if(data[i].id == userid){
                objAdmin += '<div class="default" data-member=' + data[i].id + '><div class="icon admin"></div><span>' + data[i].login + '</span></div>';
            } else {
                objAdmin += '<div class="default" data-member=' + data[i].id + '><div class="icon admin"></div><span>' + data[i].login + '</span><button class="add-friend" id="btn-addFriend"></button></div>';
            }
        } else {
            if(data[i].id == userid){
                obj += '<div class="default" data-member='+ data[i].id +'><div class="icon membre"></div><span>'+ data[i].login +'</span></div>';
            } else {
                obj += '<div class="default" data-member='+ data[i].id +'><div class="icon membre"></div><span>'+ data[i].login +'</span><button class="add-friend" id="btn-addFriend"></button></div>';
            }
        }
    }

    asideCollaborators.empty();
    asideCollaborators.append(objAdmin).append(obj);
};

var checkUsersProperty = function(aside, requests, friends){
    var asideCollaborators = aside.find('#collaborators'),
        collaborators = asideCollaborators.children();

    for(var i = 0 ; i < requests.length ; i++){
        collaborators.each(function(){
            var $collaborator = $(this);
            var id = $collaborator.attr('data-member');

            if(requests[i].id == id){
                changeIconToRequestPending($collaborator);
            }
        });
    }

    for(var j = 0; j < friends.length; j++){
        collaborators.each(function(){
            var $collaborator = $(this);
            var id = $collaborator.attr('data-member');

            if(friends[j].id == id){
                changeIconToRequestAccept($collaborator);
            }
        });
    }
};

var changeIconToRequestPending = function(div){
    var obj = '<div class="request-pending"></div>';
    div.find('button.add-friend').remove();
    div.append(obj);
};

var changeIconToRequestAccept = function(div){
    var obj = '<div class="request-accept"></div>';
    div.find('button.add-friend').remove();
    div.find('div.request-pending').remove();
    div.append(obj);
};

// --------------------------------------------- //
// -----  FONCTION ALIMENTE PROJET ACTIONS ----- //
// --------------------------------------------- //

var updateCurrentActions = function(nbActions){
    var spanNbActions = $('#nb_current_actions');
    var currentActions = spanNbActions.attr('data-action');
    var updateActions = parseInt(currentActions) + parseInt(nbActions);

    if(updateActions == 0){
        spanNbActions.html('C\'est le néant');
    } else {
        spanNbActions.html(updateActions);
    }
};

var updateFinishedActions = function(nbActions){
    var spanNbActions = $('#nb_finish_actions');
    var currentActions = spanNbActions.attr('data-action');
    var updateActions = parseInt(currentActions) + parseInt(nbActions);

    if(updateActions == 0){
      spanNbActions.html('C\'est le néant');
    } else {
        spanNbActions.html(updateActions);
    }
};

var updateTitleProjetAside = function(title){
    $('#title_project').html(title);
};

var updateDescriptionProjetAside = function(projet){
    var $description = $('#team-info');
    $description.html(projet[0].description);
};

var setRedirectUrl = function(projet_id){
    var parser = window.location.href;
    parser = parser.split('/');

    var URL_ACTIONS_COMPLETE = '/'+ parser[3] +'/projets/'+ projet_id + '/actions/complete';
    var URL_PROJET_EDIT = '/'+ parser[3] +'/projets/' + projet_id + '/priority';

    $('#btn-complete').attr('href', URL_ACTIONS_COMPLETE);
    $('#btn-edit').attr('href', URL_PROJET_EDIT);
};


// --------------------------------------------- //
// --------  FONCTION POUR CLICK DROIT  -------- //
// --------------------------------------------- //
var rightClickBuild = function(priorities){
    var todo = $('div#actions-wrapper'),
        $actions = todo.find('label'),
        $obj = '<div class="actions-priority"><ul>',
        options = '';

    if(priorities) {
        for (var i = 0; i < priorities.length; i++) {
            options += '<li id="priority-'+ priorities[i].id +'" data-id="'+ priorities[i].id +'">'+ priorities[i].libelle +'</li>'
        }

        $obj += options;
        $obj += '</ul></div>';
    }

    $actions.each(function(){
        var $this = $(this),
            parent = $this.parent();

        if(!parent.hasClass('binding')){
            $this.on('contextmenu', function(e) {
                e.preventDefault();
                var action = $this.closest('div.action'),
                    idPriority = action.attr('data-priority');

                var parent = $this.parent(),
                    $priority = $('div.actions-priority');

                if($priority.length > 0) {
                    if(parent.hasClass('current')){
                        $priority.remove();
                        parent.removeClass('current');
                    } else {
                        $priority.remove();
                        $('div.action').removeClass('current');
                        parent.addClass('current');
                        parent.append($obj);
                    }
                } else {
                    parent.append($obj);
                    parent.addClass('current');
                }

                $('li#priority-'+ idPriority).addClass('is-active');
                return false;
            });
        }
        parent.addClass('binding');
    });
};

var parserUrl = function(){
    var parser = document.createElement('a');
    parser.href = window.location.href;
    return parser;
};

// --------------------------------------------- //
// ------  ALIMENTES LES PRIOS DU PROJET  ------ //
// --------------------------------------------- //
var setFilterOptions = function(data){
    var $ul     = $('#priority-query'),
        options  = '';
    for (var i = 0; i < data.length; i++) {
        options += '<li class="query" id="priority-'+ data[i].id +'" data-id="'+ data[i].id +'">'+ data[i].libelle +'</li>'
    }

    $ul.empty();
    $ul.append(options);
    return true;
};

var checkProjetProperty = function(idAdmin, userid){
    if(idAdmin == userid){
        $('button#btn-delete-projet').css('display', 'block');
    }
};

// Ready for Turbolinks
var ready;

ready = function () {

    // ---------------------- //
    // ------ VARIABLES ----- //
    // ---------------------- //
    var userid              = $('#userid').attr('data-user'),
        currentProjet       = '',
        formAddAction       = $('#formAddAction'),
        todolist            = $('#actions-wrapper'),
        $btnAjax            = $('.btn-ajax'),
        $btnFilter          = $('#btn-filter'),
        $btnEdit            = $('#btn-edit'),
        divToRemove         = [],

    // ul - li transform on select
        $btnProject         = $('.btn-project'),
        divProjectList      = $('#projects'),
        ulProjectList       = divProjectList.find('ul li'),
        divAcceptUser       = $('#select-users'),
        userTarget          = $('#select-users span'),

    // manage team
        userlists           = $('#userlists li'),
        mainContent         = $('#display'),
        formAddProjet       = $('#formAddProjet'),
        tabAffectations     = [],
        createContent       = $('div#display-default'),

    // Team details
        divFilter           = $('#todolist-query'),
        $btnDetails         = $('#btn-details'),
        $asideDetails       = $('#aside'),

    // Friends
        $divFriends         = $('#friends'),
        $bgColor            = $('#bg-color'),
        $btnFriends         = $('#btn-friends'),
        $btnMessage         = $('#btn-message'),
        $messageBlock       = $('#message-block'),
        $btnClose           = $('button.btn-close'),
        $btnRequest         = $('#btn-request'),
        userToRemove        = '',

    // Popin
        $btnDeleteFriends   = $('#btn-delete-friend'),
        $btnAddFriend       = $('button#btn-addFriend'),
        $popinAdd           = $('.popin-add'),
        $popinDelete        = $('.popin-delete'),
        $targetUserForMail  = '',

    // Priority
        $tablePriority      = $('#table-priority'),
        $btnNewPriority     = $('button#btn-new-priority'),
        $btnSavePriority    = $('button#btn-save-priority'),
        $btnDeletePriority  = $('button#btn-delete-priority'),

    // Filtre
        $formSearchByLogin  = $('#formSearchByLogin'),

    // Flash
        $flash              = $('#notice');

    /******************************************/
    /********* Event click btn friends ********/
    /******************************************/

    window.setInterval(function(){
        var $requestWrapper = $('#request-wrapper'),
            $ul             = $requestWrapper.find('ul');
        if($ul.children().length > 0){
            if(!$requestWrapper.hasClass('is-active')){
                $('#btn-request').toggleClass('ping');
            }
        }
    }, 500);

    $('body').on('click', 'button#btn-addFriend', function(e){
        var $this       = $(this),
            $parent     = $this.parent(),
            $user_to    = $parent.attr('data-member');

        if(!$this.hasClass('request-pending')) {
            $.ajax({
                url: '/users/add/relation',
                method: 'POST',
                data: {
                    send_to: $user_to
                },
                dataType: 'json',
                success: function (data) {
                    changeIconToRequestPending($parent);
                    if ($popinAdd.hasClass('is-active')) {
                        $bgColor.removeClass('is-active');
                        $popinAdd.removeClass('is-active');

                        changeIconToRequestPending($collaborator);
                    } else {
                        $("html, body").animate({scrollTop: 0}, "fast");
                        $bgColor.addClass('is-active');
                        $popinAdd.addClass('is-active');
                    }
                }
            });
        } else {
            alert('Une demande a déjà été envoyée');
            return false;
        }
    });

    $('body').on('click', 'button#btn-accept-request', function(e){
        var $this           = $(this),
            $li             = $this.parent(),
            $pseudo         = $li.find('span').html(),
            $he_invite_me   = $this.attr('data-user');

        var obj = '<li class="friend" id="friend-'+ $he_invite_me +'"data-user="'+ $he_invite_me +'"><span class="pseudo">'+ $pseudo +'</span><button data-user="'+ $he_invite_me +'" class="delete" id="btn-delete-friend"></button><button data-user="'+ $he_invite_me +'" class="message" id="btn-message"></button></li>';

        var friendsList = $('ul.my-friends');

        $.ajax({
            url     :   '/users/confirm/relation',
            method  :   'PUT',
            data    :   {
                he_invite_me : $he_invite_me
            },
            dataType:   'json',
            success :   function(data){
                $li.slideUp();
                friendsList.append(obj);
            }
        });
    });


    $('body').on('click', 'button#btn-delete-request', function(e){
        var $this           = $(this),
            $li             = $this.parent(),
            $he_invite_me   = $this.attr('data-user');

        $.ajax({
            url     :   '/users/delete/request',
            method  :   'DELETE',
            data    :   {
                he_invite_me : $he_invite_me
            },
            dataType:   'json',
            success :   function(data){
                if(data.status == 'success'){
                    $li.slideUp();
                }
            }
        });
    });

    $('button.btn-yes').on('click', function(e){
        var $popin = $(this).closest('.popin');
        $popin.removeClass('is-active');

        $.ajax({
            url     :   '/users/delete/relation',
            method  :   'DELETE',
            data    :   {
                user : userToRemove
            },
            dataType:   'json',
            success :   function(data){
                if(data.status == 'success') {
                    $('#friend-' + userToRemove).slideUp();
                    $bgColor.removeClass('is-active');
                }
            }
        });
    });

    $('body').on('click', 'button#btn-delete-friend', function(e){
        if($popinDelete.hasClass('is-active')){
            $bgColor.removeClass('is-active');
            $popinDelete.removeClass('is-active');
        } else {
            $("html, body").animate({ scrollTop: 0 }, "fast");
            $bgColor.addClass('is-active');
            $popinDelete.addClass('is-active');
        }
        userToRemove = $(this).attr('data-user');
    });

    $btnClose.on('click', function(e){
        var $popin = $(this).closest('.popin');
        $popin.removeClass('is-active');
    });

    $btnRequest.on('click', function(e){
        var $this           = $(this),
            $requestWrapper = $this.closest('div.request-wrapper');

        var $requestWrapper = $('#request-wrapper'),
            $ul             = $requestWrapper.find('ul');

        if($requestWrapper.hasClass('is-active')){
            $requestWrapper.removeClass('is-active');
        } else {
            $requestWrapper.addClass('is-active');
        }
    });

    $btnFriends.on('click', function(e){
        var $this       = $(this),
            $friends    = $this.closest('div.friends');

        $friends.toggleClass('is-active');
    });

    $('body').on('click', 'button#btn-message', function(e){
        if($messageBlock.hasClass('is-active')){
            $bgColor.removeClass('is-active');
            $messageBlock.removeClass('is-active');
        } else {
            $targetUserForMail = $(this).parent().find('span.pseudo').attr('data-mail');
            $messageBlock.find('input[name="pseudo"]').val($targetUserForMail);
            $messageBlock.find('input[name="objet"]').focus();
            $("html, body").animate({ scrollTop: 0 }, "fast");
            $bgColor.addClass('is-active');
            $messageBlock.addClass('is-active');
        }
    });

    $messageBlock.on('submit', function(e){
        var $this       = $(this),
            $mail       = $this.find('input[name="pseudo"]').val(),
            $objet      = $this.find('input[name="objet"]').val(),
            $message    = $this.find('textarea').val();

        $.ajax({
            url     : '/users/send/mail',
            method  : 'POST',
            data    : {
                mail    : $mail,
                title   : $objet,
                message : $message
            },
            success : function(data){
                console.log(data);
                if(data.status == 'success'){
                    $bgColor.removeClass('is-active');
                    $messageBlock.removeClass('is-active');
                    flashMessage($flash, data.flash, data.status)
                }
            }
        });

        return false;
    });

    $btnClose.on('click', function(e){
        $bgColor.removeClass('is-active');
        $messageBlock.removeClass('is-active');
    });

    /******************************************/
    /********* Event click btn project ********/
    /******************************************/
    $btnProject.on('click', function(e){
        var btn = $(this);

        if(btn.hasClass('is-active')){
            btn.removeClass('is-active');
            divProjectList.slideUp();
        } else {
            btn.addClass('is-active');
            divProjectList.slideDown();
        }
    });

    /******************************************/
    /********* Event click project list *******/
    /******************************************/
    ulProjectList.on('click', function (e) {
        var $this   = $(this),
            $html   = $this.html(),
            $id     = $this.attr('id');

        var current = $('#current');

        current.attr('data-key', $id);
        current.html($html);

        $btnProject.removeClass('is-active');
        divProjectList.slideUp();

        if($id == "default"){
            mainContent.removeClass('on');
            createContent.removeClass('off');
            $asideDetails.removeClass('on');
        } else {
            mainContent.addClass('on');
            createContent.addClass('off');
            $asideDetails.addClass('on');

            $.ajax({
                url         : '/actions/' + $id,
                method      : 'GET',
                dataType    : 'json',
                success     : function(data){
                    console.log(data);
                    var idAdmin = data[0].admin[0].created_by_id;
                    currentProjet = $id;

                    // Alimente la todolist avec les actions
                    appendToTodolist(todolist, data[0].actions);

                    // Alimente l'aside
                    appendToProjetDetail($asideDetails, data[0].users, idAdmin, userid);

                    // Update compteur d'actions
                    updateCurrentActions(data[0].nb_current_action);
                    updateFinishedActions(data[0].nb_ended_action);

                    // Update titre de l'aside
                    updateTitleProjetAside($html);
                    updateDescriptionProjetAside(data[0].projet);

                    // Set URL pour les actions complètes
                    setRedirectUrl($id);

                    // Add ContextMenu to todolist
                    rightClickBuild(data[0].priorities);

                    // Inject queries filter
                    setFilterOptions(data[0].priorities);

                    // Regarde si on a des amis ou invitation
                    checkUsersProperty($asideDetails, data[0].requests, data[0].friends);

                    // Check si on est l'admin du projet
                    checkProjetProperty(idAdmin, userid);
                }
            });
        }
    });

    /******************************************/
    /********** Event click btn filter ********/
    /******************************************/

    $btnFilter.on('click', function (e) {
        if(divFilter.hasClass('is-active')){
            $btnFilter.removeClass('is-active');
            divFilter.removeClass('is-active');
            divFilter.slideUp();
        } else {
            divFilter.addClass('is-active');
            $btnFilter.addClass('is-active');
            divFilter.slideDown();
        }
    });

    $formSearchByLogin.on('submit', function(e){
        var $this       = $(this),
            queryLogin  = $this.find('input[type=text]').val();

        $.ajax({
        });

    });

    // Filter by nickname


    /******************************************/
    /********* Event click btn details ********/
    /******************************************/

    $btnDetails.on('click', function(){
        var $this = $(this);
        if($this.hasClass('is-active')){
            $this.removeClass('is-active');
            $asideDetails.removeClass('is-active');
        } else {
            $this.addClass('is-active');
            $asideDetails.addClass('is-active');
        }
    });

    /******************************************/
    /*********** Event click userlist *********/
    /******************************************/

    userlists.on('click', function (e) {
        var user    = $(this),
            name    = user.html(),
            id      = user.attr('id');

        var divAcceptUser = $('#select-users'),
            obj = "<span data-id="+ id +">"+ name +"</span>";

        divAcceptUser.append(obj);
        user.addClass('is-adding');
        return false;
    });

    /******************************************/
    /************* Event right click **********/
    /******************************************/

    rightClickBuild();

    /*******************************************/
    /******** Event remove from userlist *******/
    /*******************************************/

    $('body').on('click', '#select-users span', function(e){
        var user    = $(this),
            id      = user.attr('data-id');

        var userTarget = $('#' + id);

        if(userTarget.hasClass('is-adding')){
            userTarget.removeClass('is-adding');
            user.remove();
        }
    });

    formAddProjet.on('submit', function(e){

        var children = divAcceptUser.children();
        children.each(function(){
           var userid = $(this).attr('data-id');
            tabAffectations.push(userid);
        });

        var input = $('input[name="projet[user_ids][]"');
        input.val(tabAffectations);

        return true;
    });



    formAddAction.on('submit', function(){
        var inputAction = $(this).find('input[type=text]').val(),
            projetId    = $('#current').data('key');

        if(inputAction.length == 0){
            return false;
        } else {

            $.ajax({
                url : '/actions/new',
                method : 'post',
                dataType : 'json',
                data : {
                    contenu : inputAction,
                    projet  : projetId
                },
                success : function(data){
                    if(data.status == 'success'){
                        var obj = "<div class='action' id='todo-"+ data.idAction +"' data-key='"+ data.idAction +"'><input type='checkbox' name='checkbox' id='checkbox-"+ data.idAction +"'><label for='checkbox-"+ data.idAction +"'>"+  inputAction +"</label></div>";
                        todolist.prepend(obj);
                        $('input#ipt-todolist').val('');
                        var cc = $('#nb_current_actions').attr('data-action');
                        cc++;
                        $('#nb_current_actions').html(cc);

                        // Add right click event on dynamic element
                        rightClickBuild();
                    }
                }
            });
        }
        return false;
    });


    $btnAjax.on('click',  function(e){
        var $this       = $(this);
        var checkbox    = $('input[type="checkbox"]:checked');

        checkbox.each(function(){
            var checkbox = $(this),
                div      = $(this).parent(),
                div_id   = div.data('key');

            divToRemove.push(div_id);
        });

        if($this.hasClass('btn-archive')){
            $.ajax({
                url     : '/actions/edit',
                method  : 'PUT',
                data    : {
                    actions : divToRemove
                },
                dataType: 'json',
                success : function(data){
                    for(var i = 0 ; i < divToRemove.length ; i++){
                        var el = $('div#todo-' + divToRemove[i]);
                        el.addClass('is-archived');
                    }
                    //console.log(data);
                }
            });
        }

        if($this.hasClass('btn-delete')){
            $.ajax({
                url     : '/actions/delete',
                type    : 'DELETE',
                dataType: 'json',
                data    : {
                    actions : divToRemove
                },
                success : function(data){
                    //console.log(data);
                    for(var i = 0 ; i < divToRemove.length ; i++){
                        var el = $('div#todo-' + divToRemove[i]);
                        el.slideUp();
                    }
                }
            });
        }
    });

    /******************************************/
    /************** Event PRIORITY ************/
    /******************************************/

    var $inputPriority = $tablePriority.find('input[type="text"]');

    $('body').on('click', 'table input[type="text"]', function(e){
        $(this).select();
    });

    $('body').on('click', 'table button#btn-new-priority',function(e){
        var tbody   = $tablePriority.find('tbody');

        var parser = parserUrl();
        var projet_id = parser.pathname.split('/')[3];

        $.ajax({
            url     : '/projets/'+ projet_id + '/priority/new',
            method  : 'POST',
            dataType: 'json',
            success : function(data){
                //console.log(data);
                var obj = "<tr id='priority-"+ data.id +"'><td><input type='text' value='...' name='priority-libelle'></td><td><input type='text' name='priority' value='...'></td><td><button class='btn-save' data-id='"+ data.id +"' id='btn-save-priority'>Enregistrer</button>&nbsp;<button class='btn-delete' data-id='"+ data.id +"' id='btn-delete-priority'>Supprimer</button></td></tr>";
                tbody.append(obj);
            }
        });
    });

    $('body').on('click', 'button#btn-delete-priority', function(e){
        var tbody   = $tablePriority.find('tbody'),
            $tr     = $(this).closest('tr'),

            parser = parserUrl(),
            projet_id = parser.pathname.split('/')[3],
            priority_id = $(this).attr('data-id');

        var URL = '/projets/'+ projet_id +'/priority/'+ priority_id +'/delete';

        $.ajax({
            url     :  URL,
            method  : 'DELETE',
            data    : {
                id  : priority_id
            },
            dataType: 'json',
            success : function(data){
                //console.log(data);
            }
        });
        $tr.remove();
    });

    $('body').on('click', 'button#btn-save-priority', function(e){
        var $tr = $(this).closest('tr'),
            $inputPriorityOrder = $tr.find('input[name="priority"]'),
            $inputPriorityLibelle = $tr.find('input[name="priority-libelle"]'),

            val,

            parser = parserUrl(),
            projet_id = parser.pathname.split('/')[3],
            priority_id = $(this).attr('data-id');

        if(parseInt($inputPriorityOrder.val())){
            val = $inputPriorityOrder.val();
        } else {
            val = 1;
        }

        var URL = '/projets/'+ projet_id +'/priority/'+ priority_id +'/edit';

        $.ajax({
            url     :  URL,
            method  : 'PUT',
            data    : {
                id  : priority_id,
                order : val,
                libelle : $inputPriorityLibelle.val()
            },
            dataType: 'json',
            success : function(data){
                if(data.status == 'success'){
                    flashMessage($flash, data.flash, data.status);
                }
            }
        });
    });

    // Set priority of actions
    $('body').on('click', '.actions-priority li', function (e) {
        var $this = $(this),
            idPriority = $this.attr('data-id');

        var action      = $(this).closest('div.action'),
            idAction    = action.attr('data-key');

        $.ajax({
            url : '/actions/'+ idAction + '/edit/priority',
            method : 'PUT',
            data : {
                id_action   : idAction,
                id_priority : idPriority
            },
            success : function(data){
                //console.log(data);
                if(data.status == 'success') {
                    var $parent = $this.parent(),
                        $li = $parent.children();

                    $li.each(function () {
                        if ($(this).hasClass('is-active')) {
                            $(this).removeClass('is-active');
                        }
                    });

                    $this.addClass('is-active');
                    flashMessage($flash, data.flash, data.status);
                }
            }
        });
    });

    // Filter
    $('body').on('click', 'li.query', function(e) {
        var $this   = $(this),
            $parent = $this.parent(),
            $id     = $parent.attr('id');

        var id_query    = $this.attr('data-id'),
            query_key   = '';

        switch($id){
            case 'date-query':
                query_key = 'date';
                break;
            case 'priority-query':
                query_key = 'priority';
                break;
        }

        var URL_REQUEST = '/q?'+ query_key + '=' + id_query;
        $.ajax({
            url     : URL_REQUEST,
            method  : 'GET',
            data    : {
                idProjet : currentProjet
            },
            dataType: 'json',
            success : function(data){
                if(data.status == 'success'){
                    appendToTodolist(todolist, data.actions);
                    //location.hash = URL_REQUEST;
                }
            }
        });
    });

    $('#formSearchByLogin').on('submit', function(e){
        var $this       = $(this),
            id_query    = $this.find('input[name="ipt-query-name"]').val(),
            query_key   = 'login';

        var URL_REQUEST = '/q?'+ query_key + '=' + id_query;

        $.ajax({
            url     : URL_REQUEST,
            method  : 'GET',
            data    : {
                idProjet : currentProjet
            },
            dataType: 'json',
            success : function(data){
                if(data.status == 'success'){
                    appendToTodolist(todolist, data.actions);
                    //location.hash = URL_REQUEST;
                }
            }
        });
        return false;
    });

    $('button#btn-delete-projet').on('click', function(e){
        var current = $('#current'),
            $li     = $('li#default'),
            $id     = $li.attr('id'),
            $html   = $li.html();

        var $ul         = divProjectList.find('ul'),
            current_li  = $ul.find('li#' + currentProjet);

        $.ajax({
            url    : '/projets/delete',
            method : 'DELETE',
            data   : {
                projet_id : currentProjet
            },
            dataType : 'json',
            success  : function(data){
                if(data.status == 'success') {
                    current.attr('data-key', $id);
                    current.html($html);
                    mainContent.removeClass('on');
                    createContent.removeClass('off');
                    $asideDetails.removeClass('on');
                    current_li.remove();
                }
            }
        });
    });
};

$(document).ready(ready);
$(document).on('page:load', ready);
