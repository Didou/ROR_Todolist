class HomeController < ApplicationController

  ## Check if @user is authenticate
  before_action :authenticate_user!


  ##
  # Load home page
  #
  ##
  def index
    @user = current_user

    @projets = Projet.get_user_affectations(@user.id)

    @action = Action.new

    @requests_pending = @user.pending_invited_by
    @requests = @user.pending_invited
    @friends = @user.friends

    UserMailer.welcome_email(@user).deliver_now
  end
end
