class ActionsController < ApplicationController

  ##
  # XHR Method
  # Create new action
  #
  # POST /actions/new
  ##
  def create
    if request.xhr?
      @action = Action.new(contenu: params[:contenu], projet_id: params[:projet], created_by_id: current_user.id, is_archived: false)

      if @action.save
        response = { 'status'   => 'success', 'idAction' => @action.id }
      else
        response = { 'status'   => 'error', }
      end

      get_response(response)
    end
  end


  ##
  # XHR Method
  # Destroy all actions define in array
  #
  # DELETE actions/delete
  ##
  def destroy
    if request.xhr?
      response = Action.get_actions_by_id(params[:actions]).delete_all()
    else
      response = -1
    end
    get_response(response)
  end


  ##
  # XHR Method
  # Load an complete array of actions, users, projet etc..
  #
  # GET /actions
  ##
  def get_actions
    if request.xhr?
      projet_id = params[:id]

      @admin              = Projet.get_admin_projet(projet_id)
      @projet             = Projet.by_projet_id(projet_id)

      @users              = User.get_users_by_projet(projet_id)

      @actions            = Action.get_actions_by_projet(projet_id)
      actions_on_working  = Action.count_actions(projet_id)
      actions_finished    = Action.count_is_finished(projet_id)

      @priorities         = Priority.get_priorities_by_projet(projet_id)

      @requests           = current_user.pending_invited
      @friends            = current_user.friends

      response = Array.new.push(
          'actions'           => @actions,
          'users'             => @users,
          'projet'            => @projet,
          'admin'             => @admin,
          'priorities'        => @priorities,
          'nb_current_action' => actions_on_working,
          'nb_ended_action'   => actions_finished,
          'requests'          => @requests,
          'friends'           => @friends
      )

      get_response(response)
    end
  end


  ##
  # XHR Method
  # Set actions to :is_archived = false
  #
  # PUT /actions/edit
  ##
  def set_archived
    if request.xhr?
      id_actions = params[:actions]
      response = Action.get_actions_by_id(id_actions).update_all(:is_archived => true)
    else
      response = -1
    end
    get_response(response)
  end


  ##
  # XHR Method
  # Set action priority
  #
  # PUT /actions/:id/edit/priority
  ##
  def set_priority
    if request.xhr?
      if Action.update(params[:id], :priority_id => params[:id_priority])
        message = t('controllers.priority.set_priority_success')
        response = { 'status'   => 'success', 'flash' => message}
      else
        response = { 'status'   => 'error'}
      end
      get_response(response)
    end
  end

  ##
  # XHR Method
  # Load actions filtered by queries [PRIORITY, DATE, USERNAME]
  #
  # GET /q
  ##
  def queries_filter
    if request.xhr?
      id_projet = params[:idProjet]
      if params[:priority]
        @actions = Action.filter_by_priority(params[:priority], id_projet)
      elsif params[:login]
        @user     = User.load_by_login(params[:login])
        if @user.present?
          @actions  = Action.filter_by_login(@user.first.id, id_projet)
        else
          @actions = []
        end
      elsif params[:date]
        @actions = Action.filter_by_date(params[:date], id_projet)
      else
        not_found
      end
      response = {
          'status'   => 'success',
          'actions'  =>  @actions
      }
      get_response(response)
    else
      not_found
    end
  end
end