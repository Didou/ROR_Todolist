class ProjetsController < ApplicationController
  attr_accessor :projet

  before_action :set_projet, only: %i(edit update)

  def index

  end

  def show

  end

  ##
  # Prepare object for new projet
  #
  # GET /projets/new
  # @return : @users
  ##
  def new
    @projet = Projet.new
    @users = User.all
  end

  ##
  # Save new projet
  #
  # POST /projets/new
  # Redirect to : root_path with notification success
  ##
  def create
    @projet = Projet.new(projet_params)
    @projet.created_by_id = current_user.id

    userids = params.require(:projet).permit(user_ids: [])
    userids = userids['user_ids'].first.split(',')

    if @projet.save
      Priority.set_default_properties(@projet.id)
      UserAffectation.assigned_to_project(userids, @projet.id)
      flash[:success] = t('controllers.projets.hello_flash', user: current_user.login, projet: @projet.name)
      redirect_to root_path
    else
      flash[:success] = ''
      render 'new'
    end
  end

  ##
  # @DEPRECATED
  # Edit projet by id
  #
  # GET /projets/1/edit
  ##
  def edit
    @users = User.all
  end


  ##
  # Delete projet by id
  #
  # DELETE /projets/delete
  ##
  def destroy
    if request.xhr?
      if Projet.destroy(params[:projet_id])
        response = { 'status'   => 'success'}
      else
        response = { 'status'   => 'error'}
      end
      get_response(response)
    end
  end


  ##
  # Load all complete action by projet
  #
  # GET /projets/1/actions/complete
  ##
  def get_actions_complete
    @actions = Action.get_complete_actions_by_projet(params[:id])
    render 'projets/archive'
  end



  ### PRIVATE ###
  private

  def set_projet
    @projet = Projet.by_projet_id(params[:id])
  end

  def projet_params
    params.require(:projet).permit(:name, :description)
  end
end