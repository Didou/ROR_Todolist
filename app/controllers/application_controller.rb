class ApplicationController < ActionController::Base
  protect_from_forgery

  ## Set locale before actions
  before_filter :set_locale

  ## Load Layout for views
  layout 'application'

  ##
  # Generic Method
  # Format response for XHR Method
  #
  # @format : JSON
  ##
  def get_response(data)
    respond_to do | format |
      format.json {
        render json: data
      }
    end
  end

  ##
  # Generic Method
  # Return 404
  #
  # @return : 404
  ##
  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end


  ### PRIVATE ###
  private

  ##
  # Module : i18l
  # Check the locale in URL, if not define :fr
  #
  ##
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  ##
  # Module : i18l
  # Define the default locale in URL
  #
  ##
  def self.default_url_options(options={})
    { :locale => I18n.locale }
  end
end