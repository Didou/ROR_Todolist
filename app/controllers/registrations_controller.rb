class RegistrationsController < Devise::RegistrationsController

  ### PRIVATE ###
  private

  ##
  # Override sign_up devise method for [:login] addition
  ##
  def sign_up_params
    params.require(:user).permit(:login, :email, :password, :password_confirmation)
  end

  ##
  # Override update devise method for [:login] addition
  ##
  def account_update_params
    params.require(:user).permit(:login, :email, :password, :password_confirmation, :current_password)
  end
end