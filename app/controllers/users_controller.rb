class UsersController < ApplicationController

  ##
  # XHR Method
  # Send request to user for friendship
  #
  # @param : user_to
  # @return : boolean
  ##
  def send_request
    if request.xhr?
      @user_from = current_user
      @user_to = User.load_by_id(params[:send_to])

      if @user_from.invite @user_to
        response = { 'status'   => 'success'}
      else
        response = { 'status'   => 'error'}
      end
      get_response(response)
    end
  end

  ##
  # XHR Method
  # Confirm request between two users
  #
  # @param : he_invite_me
  # @return : boolean
  ##
  def confirm_request
    if request.xhr?
      @user_invite_me = User.load_by_id(params[:he_invite_me])
      @current_user = current_user

      if @current_user.approve @user_invite_me
        response = { 'status'   => 'success'}
      else
        response = { 'status'   => 'error'}
      end
      get_response(response)
    end
  end

  ##
  # XHR Method
  # Delete relation between two user
  #
  # @param : user_id
  # @return : boolean
  ##
  def delete_relation
    if request.xhr?
      @user_1 = current_user
      @user_2 = User.load_by_id(params[:user])

      if @user_1.remove_friendship @user_2
        response = { 'status'   => 'success'}
      else
        response = { 'status'   => 'error'}
      end
      get_response(response)
    end
  end


  ##
  # XHR Method
  # Don't accept request from user_one
  #
  # @param : user_id
  # @return : boolean
  ##
  def delete_request
    if request.xhr?
      @user_invite_me = User.load_by_id(params[:he_invite_me])
      @current_user = current_user

      @current_user.approve @user_invite_me

      if @current_user.remove_friendship @user_invite_me
        response = { 'status'   => 'success'}
      else
        response = { 'status'   => 'error'}
      end
      get_response(response)
    end
  end


  ##
  # XHR Method
  # Send mail from user 1 to user 2
  #
  # @param : mail
  # @param : objet
  # @param : content
  #
  # @return : boolean
  ##
  def send_email
    if request.xhr?
      UserMailer.new_email(current_user, params[:mail], params[:title], params[:message]).deliver_now
      user_to = User.load_by_mail(params[:mail])

      @log = LogMailing.new(objet: params[:title], message: params[:message], users_from: current_user.id, users_to: user_to.id)
      if @log.save
        message = t('mailers.user.welcome_message', user: user_to.login)
        response = { 'status'   => 'success', 'flash' => message}
      else
        response = { 'status'   => 'error'}
      end
      get_response(response)
    end
  end
end
