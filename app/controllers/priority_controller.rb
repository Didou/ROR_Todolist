class PriorityController < ApplicationController

  ##
  # XHR Method
  # Load all priorities of projet
  #
  # GET /projets/1/priority
  # @return : @priorities
  ##
  def index
    projet_id = params[:id]
    @priorities = Priority.get_priorities_by_projet(projet_id)

    @user = current_user
    @requests_pending = @user.pending_invited_by
    @requests = @user.pending_invited
    @friends = @user.friends

    if @priorities.length < 1
      @priorities = Priority.get_default_priorities
    end
  end


  ##
  # XHR Method
  # Create new priority
  #
  # POST /projets/1/priority/new
  # @return : @priority
  ##
  def create
    projet_id = params[:id]
    @priority = Priority.new(projet_id: projet_id).save
    @priority = Priority.last
    get_response(@priority)
  end


  ##
  # XHR Method
  # Update priority load by id
  #
  # PUT /projets/1/priority/1/edit
  ##
  def update
    if request.xhr?
      res = Priority.update(params[:id], :libelle => params[:libelle], :order => params[:order])
      if res
        message = t('controllers.priority.set_priority_data')

        response = { 'status'   => 'success', 'flash' => message}
      else
        response = { 'status'   => 'error' }
      end
      get_response(response)
    end
  end


  ##
  # XHR Method
  # Delete priority load by id
  #
  # DELETE /projets/1/priority/1/destroy
  ##
  def destroy
    if request.xhr?
       if Priority.destroy(params[:id])
         response = { 'status'   => 'success'}
       else
         response = { 'status'   => 'error'}
       end
       get_response(response)
    end
  end
end