class Priority < ActiveRecord::Base
  belongs_to :projet

  ### Scopes ###

  ##
  # Load default properties to new projet
  #
  # @return : @projet
  ##
  def self.get_default_priorities
    where(projet_id: 0)
  end


  ##
  # Load the priorities of projet
  #
  # @param : projet_id
  # @return : @priorities
  ##
  def self.get_priorities_by_projet(projet_id)
    where(projet_id: projet_id).order(order: :desc)
  end

  ##
  # Set default properties to new projet
  #
  # @param : projet_id
  ##
  def self.set_default_properties(projet_id)
    priorities = [
        ['Faible', '1', projet_id],
        ['Moyenne', '2', projet_id],
        ['Haute', '3', projet_id],
    ]
    priorities.each do | libelle, order, projet |
      self.create(libelle: libelle, order: order, projet_id: projet)
    end
  end


  ##
  # Load priority by id
  #
  # @param : id
  ##
  def self.get_by_id(id)
    where(id: id)
  end
end
