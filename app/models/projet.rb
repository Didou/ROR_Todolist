class Projet < ActiveRecord::Base
  # Validations
  validates  :name,
             presence: true

  # Mapping relation
  has_many :users, through: :user_affectations
  ### Drop cascade ###
  has_many :user_affectations, :dependent => :delete_all
  has_many :actions, :dependent => :delete_all
  has_many :priority, :dependent => :delete_all

  ### Scopes ###

  ##
  # Load projet by projet_id
  #
  # @param : projet_id
  # @return : @projet
  ##
  def self.by_projet_id(projet_id)
    where(id: projet_id)
  end

  ##
  # Get the admin id of projet
  #
  # @param : projet_id
  # @return : int
  ##
  def self.get_admin_projet(projet_id)
    select('created_by_id').where(id: projet_id)
  end


  ##
  # Get the project affected to user
  #
  # @param : user_id
  # @return : @projets
  ##
  def self.get_user_affectations(user_id)
    joins(:user_affectations).where(:user_affectations => { user_id: user_id})
  end
end
