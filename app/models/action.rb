class Action < ActiveRecord::Base
  # Validations
  validates :contenu,
            :presence => true

  ### Relations ###
  belongs_to :created_by
  belongs_to :projet

  ### Scopes ###

  ##
  # Load actions by id
  #
  # @param : id
  # @return : @action
  ##
  def self.get_actions_by_id(id)
    where(:id => id)
  end


  ##
  # Load actions by projet_id
  #
  # @param : projet_id
  # @return : @actions
  ##
  def self.get_actions_by_projet(projet_id)
    where(projet_id: projet_id)
  end


  ##
  # Load actions by projet_id and already complete
  #
  # @param : projet_id
  # @return : @actions
  ##
  def self.get_complete_actions_by_projet(projet_id)
    where(projet_id: projet_id).where(:is_archived => true).order(updated_at: :desc)
  end


  ##
  # Count actions non complete by projet_id
  #
  # @param : projet_id
  # @return : int
  ##
  def self.count_actions(projet_id)
    where(projet_id: projet_id).where(:is_archived => false).count
  end


  ##
  # Count actions complete by projet_id
  #
  # @param : projet_id
  # @return : int
  ##
  def self.count_is_finished(projet_id)
    where(projet_id: projet_id).where(:is_archived => true).count
  end


  ##
  # Filter actions with projet_id and priority_id
  #
  # @param : id_projet
  # @param : id_priority
  # @return : @actions
  ##
  def self.filter_by_priority(id_priority, id_projet)
    where(:projet_id => id_projet).where(:priority_id => id_priority).where(:is_archived => false)
  end


  ##
  # Filter actions with id_user and id_projet
  #
  # @param : id_user
  # @param : id_projet
  # @return : @actions
  ##
  def self.filter_by_login(id_user, id_projet)
    where(:projet_id => id_projet).where(:created_by_id => id_user).where(:is_archived => false)
  end


  ##
  # Filter actions with date[ASC / DESC] and id_projet
  #
  # @param : query
  # @param : id_projet
  # @return : @actions
  ##
  def self.filter_by_date(query, id_projet)
    where(:projet_id => id_projet).where(:is_archived => false).order(created_at: query)
  end

  ###  PRIVATE METHODS ###
  private
  def archived_false
    self.is_archived = false
  end
end
