class UserAffectation < ActiveRecord::Base
  belongs_to :user
  belongs_to :projet

  # Scopes

  ##
  # Inject data in UserAffectation [user - projet]
  #
  # @param : projet_id
  # @param : users_ids
  #
  # @return : bool
  ##
  def self.assigned_to_project(user_ids, projet_id)
    ActiveRecord::Base.transaction do
      user_ids.each do | id |
        UserAffectation.new(user_id: id, projet_id: projet_id).save
      end
    end
  end
end
