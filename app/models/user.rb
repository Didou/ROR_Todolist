class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Include Amistad
  include Amistad::FriendModel

  # Relations
  has_many :user_affectations
  has_many :projets, through: :user_affectations

  ### Scopes ###

  ##
  # Load users by projet
  #
  # @param : projet_id
  # @return : @users
  ##
  def self.get_users_by_projet(projet_id)
    joins(:user_affectations).where(:user_affectations => {projet_id: projet_id})
  end


  ##
  # Load user by login
  #
  # @param : login
  # @return : @user
  ##
  def self.load_by_login(login)
    where(:login => login)
  end

  ##
  # Load user by id
  #
  # @param : id
  # @return : @user
  ##
  def self.load_by_id(id)
    find(id)
  end

  ##
  # Load user by email
  #
  # @param : email
  # @return : @user
  ##
  def self.load_by_mail(email)
    find_by(email: email)
  end
end
