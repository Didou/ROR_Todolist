class ApplicationMailer < ActionMailer::Base
  default from: 'didier.youn@gmail.com'
  append_view_path('#{Rails.root}/app/views/user_mailer')

end

