class UserMailer < ApplicationMailer
  default from: 'didier.youn@gmail.com'

  ##
  # Tutorial method from Rails doc
  #
  # @param : user
  # @return :
  ##
  def welcome_email(user)
    @user = user
    @url = 'http://local.dev:3000/fr'
    mail(to: @user.email, subject: 'Welcome to my site')
  end


  ##
  # Send new mail to user
  #
  # @param : user
  # @param : mail
  # @param : objet
  # @param : message
  ##
  def new_email(user, mail, objet, message)
    @user = user
    @message = message

    mail(to: mail, subject: objet)
  end
end