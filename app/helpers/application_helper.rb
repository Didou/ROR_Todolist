module ApplicationHelper

  ##
  # Override devise class for Todolist alert message
  #
  # @param : name
  # @return : string
  ##
  def set_class(name)
    classe = case name
       when 'alert' then 'danger'
       when 'notice' then 'success'
       else name
     end
  end

  def set_default_priorities(priority)
    case priority
      when 'Haute' then 'High'
      when 'Moyenne' then 'Medium'
      when 'Faible' then 'Low'
      else priority
    end
  end
end
