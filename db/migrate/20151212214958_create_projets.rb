class CreateProjets < ActiveRecord::Migration
  def change
    create_table :projets do |t|
      t.string :name
      t.text :description
      t.references :created_by

      t.timestamps null: false
    end
  end
end
