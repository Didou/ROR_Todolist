class CreatePriorities < ActiveRecord::Migration
  def change
    create_table :priorities do |t|
      t.string :libelle
      t.integer :order
      t.references :projet, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
