class CreateActions < ActiveRecord::Migration
  def change
    create_table :actions do |t|
      t.text :contenu
      t.boolean :is_archived
      t.references :created_by, index: true, foreign_key: true
      t.references :projet, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
