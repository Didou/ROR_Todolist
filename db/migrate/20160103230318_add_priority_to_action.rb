class AddPriorityToAction < ActiveRecord::Migration
  def change
    add_reference :actions, :priority, index: true, foreign_key: true
  end
end
