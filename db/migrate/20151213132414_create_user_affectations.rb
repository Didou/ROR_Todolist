class CreateUserAffectations < ActiveRecord::Migration
  def change
    create_table :user_affectations do |t|
      t.references :user, index: true, foreign_key: true
      t.references :projet, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
