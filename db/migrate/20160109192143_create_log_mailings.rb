class CreateLogMailings < ActiveRecord::Migration
  def change
    create_table :log_mailings do |t|
      t.string :objet
      t.text :message
      t.integer :users_from
      t.integer :users_to

      t.timestamps null: false
    end
  end
end
