# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#priorities = [
#    ['Faible', '1', '0'],
#    ['Moyenne', '2', '0'],
#    ['Haute', '3', '0'],
#]
#
#priorities.each do | libelle, order, projet |
#  Priority.create(libelle: libelle, order: order, projet_id: projet)
#end