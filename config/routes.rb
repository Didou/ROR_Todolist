Rails.application.routes.draw do

  # Préfixe l'URL par la locale
  scope '(:locale)', :locale => /en|fr/ do

    # Routes need authentification
    authenticate :user do
      root :to => 'home#index'
      get 'home/index'

      # Routes for projet edit/new
      get     '/projets'           => 'projets#index'
      get     '/projets/new'       => 'projets#new',                as: 'new_projets'
      post    '/projets/new'       => 'projets#create',             as: 'create_projets'
      get     '/projets/:id/edit'  => 'projets#edit',               as: 'edit_projet'
      patch   '/projets/:id'       => 'projets#update',             as: ''
      put     '/projets/:id'       => 'projets#update',             as: ''
      delete  '/projets/delete'    => 'projets#destroy'

      # Routes for actions new/delete
      post    '/actions/new'                => 'actions#create',             as: 'create_actions'
      get     '/actions/:id'                => 'actions#get_actions',        as: 'get_actions'
      delete  '/actions/delete'             => 'actions#destroy'
      put     '/actions/edit'               => 'actions#set_archived'        # Set is-archived a TRUE
      put     '/actions/:id/edit/priority'  => 'actions#set_priority'        # Set priority

      # Routes for actions of projet
      get     '/projets/:id/actions/complete' => 'projets#get_actions_complete', as: 'complete_actions'

      # Routes for priorities
      get     '/projets/:id/priority'             => 'priority#index'
      post    '/projets/:id/priority/new'         => 'priority#create',     as: 'create_priority'
      put     '/projets/:id/priority/:id/edit'    => 'priority#update',     as: 'edit_priority'
      delete  '/projets/:id/priority/:id/delete'  => 'priority#destroy',    as: 'destroy_priority'

      # Routes for Amistad
      post    '/users/add/relation'               => 'users#send_request'
      put     '/users/confirm/relation'           => 'users#confirm_request'
      delete  '/users/delete/relation'            => 'users#delete_relation'
      delete  '/users/delete/request'             => 'users#delete_request'

      # Routes for sending mail
      post    '/users/send/mail'                  => 'users#send_email'

      # Routes for query filters
      get     '/:q'                               => 'actions#queries_filter'
    end

    # Override controller
    devise_for :users, :controllers => { registrations: 'registrations' }
  end

end
