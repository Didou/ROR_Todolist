require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Todolist
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Paris'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', 'models', '*', '*.yml').to_s]
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', 'views', '*', '*.yml').to_s]
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', 'controllers', '*', '*.yml').to_s]
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', 'mailers', '*', '*.yml').to_s]
    config.i18n.default_locale = :fr
    config.i18n.available_locales = [:fr, :en]

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # Config for mail
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.default_url_options = { :host => 'local.dev:3000' }

    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
        address: 'didier.youn@gmail.com',
        port: '587',
        domain: 'gmail.com',
        authentification: 'plain',
        enable_starttls_auto: true,
        user_name: 'did94350',
        password: 'hermichounou'
    }
  end
end
