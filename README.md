# Todolist #

## Note pour Rémy : 
Lors de vos tests, pensez à créer plusieurs utilisateurs avant la création d'un projet ! Je n'ai pas eu le temps de développer une fonctionnalité permettant d'ajouter des collaborateurs
à un projet existant. Actuellement on alimente un projet en utilisateurs que lors de sa création. 

## Repo Gitlab : 
- SSH : [git@gitlab.com:Didou/ROR_Todolist.git](git@gitlab.com:Didou/ROR_Todolist.git)
- HTTP : [https://gitlab.com/Didou/ROR_Todolist.git](https://gitlab.com/Didou/ROR_Todolist.git)

## Contact : 
- Adresse mail : didier.youn@gmail.com

## Description du projet :
> Todolist est une application web consistant à gérer les tâches à faire dans la journée au sein d’une équipe. 
<br /><br />
Aujourd’hui la plupart des projets web sont réalisés par une équipe de développeurs, de graphistes, chef de projets et coordonner les tâches entre chacune des ressources devient vite un casse-tête pour un chef de projet devant déjà gérer les échanges clients.
<br /><br />
Ainsi, la mise en place d'outils collaboratifs devient rapidement une priorité au sein d'un projet afin de fluidifier le flux d'informations entre les différentes ressources.
<br />
Une bonne circulation de l'information permet de donner un gain de visibilité sur les tâches en cours, d'informer sur l'avancement du projet et permet au chef de projet de se maintenir à jour.
<br />
Ces différents outils ont donc un impact direct sur une bonne conduite de projet.
<br /><br />
> Todolist est donc une application collaborative permettant de faire "tampon" dans les échanges entre les différentes ressources.
<br />
Elle permet par exemple à un développeur et à un graphiste de définir un périmètre commun sur les tâches à faire dans la journée. 
<br />
Etablir ce périmètre permet déjà d'adapter les objectifs de la journée en prenant en compte le niveau de chacun mais aussi de permettre à chacun des acteurs de voir l'avancement
des travaux de ces collègues. 
<br /><br />
A terme, Todolist serait une application qui pourrait me servir dans mes projets scolaires afin de mieux organiser le flux des travaux avec les autres membres du groupe.
<br />
Néanmoins malgré les nombreuses fonctionnalités déjà développées, l'application n'est pas encore assez mature pour être utiliser de façon aussi concrète.

## Internationalisation (**i18n**) :
>
La locale utilisée par défaut est l'anglais.
<br /><br />
**Gem utilisée** : celle utilisée par défaut soit i18n.
<br /><br />
Todolist est disponible sous deux locales : le français (FR) et l'anglais UK (en)
<br />
La locale est modifiable de deux façons :
- Depuis le header via les différentes locales disponibles sous forme de bouttons.
- Depuis la barre du navigateur où chaque URL de l'application sont préfixés par la locale choisie.
```
https://local.dev:3000/fr/[etc...]
https://local.dev:3000/en/[etc...]
```
<br />
Les deux locales disponibles sont [fr] et [en].
>


## Technologies utilisées :
**Partie Front :**
```
- HTML5 
- CSS3
- Javascript
- jQuery
```
**Partie Back :**
```
- Ruby
- Ruby On Rails
```

## Gems utilisées :
>
1. **Devise**       : Authentification, gestion des utilisateurs
2. **Simple_form**  : Gestion des formulaires avec plusieurs models 
3. **Awesome_print** : Gem permettant d'indenter les tableaux depuis le shell ruby
4. **Quiet Assets** : Gem utilisée afin de "skipper" les requêtes liées aux appels médias/css/js depuis le shell ruby afin de mieux visualiser les requêtes HTTPS et ceux liés aux models
5. **Amistad** : Gem utilisée afin de gérer les amitiés entre les contacts
>

## Fonctionnalités développées : 

**Utilisateurs**
>
L'utilisation de Devise permet l'utilisation des fonctionnalités de bases d'un utilisateur soit :
- Inscription
- Connexion
- Déconnexion
- Changement du mot de passe 
- etc ... 

**Projet**
>
- Création d'un projet
- Suppression du projet
- Edition du projet [DEPRECATED]

**Actions**
>
- Ajout
- Archivage
- Suppression

**Priorité**
>
- Création de nouvelles priorités
- Modification de l'ordre d'importance des priorités
- Suppression d'une priorité
- Attribution d'une table de priorités à un projet

**Filtre**
>
- Recherche en fonction de la date (ancienne / récente)
- Recherche en fonction de la table de priorités
- Recherche en fonction d'un pseudo

**Projet - Utilisateurs**
>
- Ajout de collaborateurs dans un projet
- Le créateur du projet prend le statut de chef de projet

**Projet - Actions**
>
- Visibilité sur les actions complétées
- Filtre

**Actions - Priorités**
>
- Attribution d'une priorité à une action

**Utilisateurs - Utilisateurs**
>
- Ajout de contact
- Suppression d'un contact
- Envoi de mails

## Optimatisation :
>
## Utilisation de l'AJAX
Todolist est une application qui a deux principales problématiques : elle doit être à la fois simple d'utilisation et complète. 
<br />
Pour se faire, la quasi totalité de l'application fonctionne sur la base des requêtes AJAX permettant ainsi une meilleure expérience utilisateur.
L'AJAX sous Rails peut-être fait de deux manières, soit on utilise la manière Ruby On Rails : 
```
<td><%= link_to "Edit", edit_product_path(product), remote: true, class: "btn btn-default" %></td>
```
Ou en faisant un appel AJAX depuis le code JS en concordance avec les routes définis dans le projet :
```
$.ajax({
    url : 'URL',
    method : 'POST',
    data : { tableau JSON },
    dataType : JSON,
    success : function(data){
        // Success
    }
});
```
>
## Utilisation des scopes
>
Afin de mieux respecter l'architecture MVC j'ai fait le choix de déplacer toutes les requêtes sur les models (where, find etc...) utiliser depuis les controllers vers les models associés.
Ces requêtes sont donc accessibles via des scopes définis à l'aide de fonction préfixer par [self]
De plus, grâce à l'utilisation des scopes, j'ai pu factorisé du code pouvant être appeler dans plusieurs controllers.
```
# models/projet.rb
def self.get_projet(projet_id)
    where(id: projet_id)
end
```
>
```
# controllers/projets_controller.rb
def index
    @projet = Projet.get_projet(params[:id])
end
```
>
## Utilisation des helpers :
>
Du fait de l'utilisation des différentes gems comme Devise, il a fallu que j'adapte le code afin d'intégrer entièrement les fonctionnalités proposées par la gem.
<br>
Comme pour d'autres frameworks, Ruby On Rails nous met à disposition un outil "Helper" pouvant être appeler que ce soit côté front / côté back (views/controllers)
<br>
Dans le cas de Todolist, l'intégration HTML et CSS nous imposait que les différentes classes de type 'alert' soit suffixé par 'success' ou 'danger'.
<br>
Sans pour autant surcharger les classes de Devise, on peut utiliser un Helper afin de changer la classe renvoyée par Devise.
```
# helpers/application_helper.rb
module ApplicationHelper
    ##
    # Override class of Devise for alert message
    #
    # @param : name
    # @return : string
    ##
    def set_class(name)
        classe = case name 
            when 'alert' then 'danger'
            when 'notice' then 'success'
            else name
        end
    end
end
```
>
## Utilisation des messages flash
Les fonctionnalités de Todolist sont majoritairement faites en AJAX et de ce fait il a fallu trouver un moyen de tenir au courant l'utilisateur des réponses renvoyées par le serveur suite aux requêtes.
<br>Pour cela, Ruby On Rails nous met à disposition un outil très sympathique qui est les messages flash.
```
flash[:success] = "Salut" 
```
Ces messages sont ensuite stockés dans la session et ne seront visible que sur la prochaine vue visitée.
>
## Utilisation des mails
Ruby On Rails étant un framework très complet, il gère nativement l'envoi de mails à travers la classe <code>ActionMailer</code>.
<br>Le fonctionnement des mails est le même que pour celui des controllers ainsi on définit des classes qui étendent d'Action Mailer et on peut y écrire notre code.
<br>Le seul changement réside dans la création de template de mails définit dans le répertoire views.
<br>Une fois la configuration des mails finalisée on peut faire les appels de mails à travers :
```
UserMailer.new_email(params[:params]).deliver_now
```
>
## Utilisation des partials :
Todolist est une application où le coeur des fonctionnalités se font depuis une seule vue. Lors du développement HTML/CSS/JS, je me suis donc retrouvé avec une page d'index contenant à la fois : 
- des données liées à un projet.
- des données liées à un utilisateur et ses contacts.
- des données liées aux priorités.
- des données liées aux actions d'un projet.
- etc... <br>
Donc en terme de maintenabilité, c'est la merde.
<br>
La solution a donc été de découper la page d'index de la même façon que les différents controllers et faire des appels de ces "partials" dans la page d'index.
<br>
Un appel de partial sans passage de paramètre : 
```
<%= render :partial => 'actions/link' %>
```
Un appel de partial avec passage de paramètre :
```
<%= render :partial => "actions/form", :locals => { :action => @action }%>
```
>

## Problèmes rencontrés : 
>
**Debug** : 
- Contrairement aux autres langages qui me sont plus familiers ça a été une belle merde de faire du debug sous Rails. 
<br />Suivre l'injection de données dans un objet côté serveur me demandait de faire du **render json : variable** à chaque fois ce qui devenait un peu chiant à la fin.
<br />Le pire reste l'affichage des objets côté client, encore maintenant je suis à la recherche du var_dump sous Rails.
>
>
**Simple Form** : 
- Lors de la création d'un projet nous avons la possibilité d'y assigner des collaborateurs. 
<br>Au départ je voulais utiliser la gem simple_form qui permet une gestion de formulaire interagissant avec plusieurs models mais cela ne marchait pas. 
<br>Après 2-3 jours d'investigation j'ai préféré enlever simple_form et gérer les injections dans la table UserAffectation moi-même.
>
**Bug** : <br>
Après plusieurs tests, je vous remonte quelques bugs graphiques / techniques liés au script Javascript et l’ajout de nouveaux éléments. Vous serez amenez à faire pas mal de F5 pour résoudre ces bugs.
- Si vous avez l’aside du projet d’ouvert et que vous acceptez un contact, son icône ne se mettra pas à jour. **SOLUTION : F5 / Changer de projet**
- Lors de l’ajout d’une nouvelle action, vous devez changer de projet où reload la page afin de lui attribuer une priorité. Lors du développement de la vue, j’avais bien pris ce point en compte et cela fonctionnait parfaitement mais en faisant l’intégration dans Rails cela ne marchait plus …
**Solution : F5 / Changer de projet**
>

## Points à améliorer : 
>
**Adopter la logique Ruby** :
- La syntaxe dans le langage Ruby est assez spéciale puisque celui-ci comprend un code où le développeur "oublie" de mettre des parenthèses lors de l'appel d'une fonction ou un if (par exemple).
<br>
Ce langage étant nouveau, je n'ai pas totalement adhéré à cette écriture et j'ai préféré mettre des parenthèses dès que je le pouvais.

```
def get_actions_complete
    / Syntaxe adoptée \
    @actions = Action.get_complete_actions_by_projet(params[:id])
    
    / Syntaxe plus dans la "logique" Ruby \
    @actions = Action.get_complete_actions_by_projet params[:id]
    
    render 'projets/archive'
end
```

>
**Test unitaires / TDD** : 
- Ruby On Rails nous permet de faire des tests TDD dès la génération des models, controllers etc... mais au départ je n'ai pas forcément penser à faire ces tests.
<br> Néanmoins je n'ai pas eu le temps de faire ces tests.
>
**Validation / Sécurité** : 
- Lors des envois de données au serveur je procède déjà à des vérification côté client via le code Javascript mais côté serveur je me suis contenté de définir des validations classiques que nous pouvons définir dans les models :
```
validates :name, presence: true, uniqueness: true
```
Alors que Rails nous met à disposition de beaucoup d'autres outils pour vérifier les données envoyées au serveur.
>
**Documentation / Normes** :
- Comme pour PHP et ses normes PSR, un projet Ruby se doit d'être documenté et respecter des normes de programmations (nom des variables, nom des méthodes, indentation etc...)
<br/>Je n'ai pas eu le temps de regarder en détail les normes d'un projet Ruby (rdoc) mais j'ai essayé de vous faire une documentation semblable à celle qu'on retrouve dans les classes PHP.
<br/>C'est-à-dire : 
<br/>
>
```
##
# Description de la méthode 
#
# @param : user_id
# @param : projet_id
#
# @return : @user 
##
def load_user_by_projet(user_id, projet_id)
  // code
end
```
>

## Et après ?
>
Comme j'ai pu le dire dans la description du projet, Todolist sera une application qui me sera sûrement utile dans des futurs projets scolaires.
<br> Néanmoins avant de l'utiliser de façon concrète j'aimerai traiter les différents retours que vous me ferez après les corrections du projets mais aussi :
- Intégrer un channel IRC
- Pouvoir répertorier les actions en différentes catégorie (FRONT - BACK - GESTION DE PROJET - DEV JS etc...)
- Permettre une authentification via les réseaux sociaux. 
<br>
Et le plus chiant : <code> L'hébergement ! </code> 
>
